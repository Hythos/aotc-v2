cis_battle_droid_squad_leader = Creature:new {
objectName = "@mob/creature_names:cis_battle_droid",
customName = "A Battle Droid Squad Leader",
mobType = MOB_ANDROID,
socialGroup = "rebel",
faction = "rebel",
level = 17,
chanceHit = 0.32,
damageMin = 160,
damageMax = 170,
baseXp = 1102,
baseHAM = 3500,
baseHAMmax = 4300,
armor = 0,
resists = {15,15,15,15,15,15,15,15,-1},
meatType = "",
meatAmount = 0,
hideType = "",
hideAmount = 0,
boneType = "",
boneAmount = 0,
milk = 0,
tamingChance = 0,
ferocity = 0,
pvpBitmask = ATTACKABLE,
creatureBitmask = PACK,
optionsBitmask = AIENABLED,
diet = HERBIVORE,

templates = {"object/mobile/battle_droid_red.iff"},
lootGroups = {
	{
		groups = {
			{group = "noob_weapons", chance = 10000000},
		},
		lootChance = 5000000,
	},
},

	-- Primary and secondary weapon should be different types (rifle/carbine, carbine/pistol, rifle/unarmed, etc)
	-- Unarmed should be put on secondary unless the mobile doesn't use weapons, in which case "unarmed" should be put primary and "none" as secondary
	primaryWeapon = "battle_droid_weapons",
	secondaryWeapon = "none",
	conversationTemplate = "",
	reactionStf = "@npc_reaction/battle_droid",
	personalityStf = "@hireling/hireling_military",
	
	-- primaryAttacks and secondaryAttacks should be separate skill groups specific to the weapon type listed in primaryWeapon and secondaryWeapon
	-- Use merge() to merge groups in creatureskills.lua together. If a weapon is set to "none", set the attacks variable to empty brackets
	primaryAttacks = merge(brawlermaster,marksmanmaster),
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(cis_battle_droid_squad_leader, "cis_battle_droid_squad_leader")
